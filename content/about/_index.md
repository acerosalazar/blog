---
date: "2015-06-20T14:02:37+02:00"
title: "About"
hidden: true
---

My name is <span class="md-alert">Felix J. Acero</span>. I am a <span class="md-alert">Software Engineer</span> and currently spend most of my time at work writing software for <span class="md-alert">Apple's</span> operating systems.
