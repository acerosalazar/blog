---
title: "Growing Object Oriented Systems Through Tests"
date: 2019-02-06T09:01:32+01:00
description: "Book Review"
draft: true
---

Feedback is fundamental to agile software practices because it helps the team stay on track. Focus on feedback allows developers to be sure they are building the right software, and gives them confidence in their ability to evolve the software at constant pace.

This book is, in essence, a book about agile, but it stands out out because it focuses on trying to map important values into concrete practices and techniques that developers can use.

The walking skeleton.

[TODO]

The first image describes the beginning of TDD project. The goal of the team in this phase is to come up with a Walking Skeleton, that is the thinest, smallest slice of functionality that can be built, tested and then delivered. This pattern was first introduced by Allistar Cockburn who defined it as: [cockburn’s definition].

Something the authors highlight about this pattern, is that it forces the development team to interact with the organization in order to discover possible organizational roadblocks that may delay the delivery of the application: “If creating a database takes 6 signatures, and two months, we would want to know that as soon as possible” [TODO: get actual quote].

Once the the TDD effort has been kickstarted, the next step is to enter the well know cycle of TDD, as shown in the image below.

Mock Objects TDD

One interesting detail of the cycle described by the authors, is how the acceptance tests play an important role in TDD. Often the literature around TDD recognizes the important of different types of tests, including acceptance testing, but then focuses almost exclusively on unit testing. In this book the authors make an effort to fit acceptance testing into the development process.

Although the authors advocate for the familiar red-green-refactor cycle, the type of testing the use was completely new to me. Like other TDD advocates, they describe TDD as a process that provides many benefits, including protection against functional regressions, reduction of bugs, specification and design. The type of testing they use delivers all these benefits, while paying particular attention to the design benefits that can come from testing.

The authors use a style of TDD called TDD with Mock Objects as described in their OOPSLA paper, although I prefer the terminology used by Fowler that refers to it as the “mockist” style. There are several advantages and disadvantages that come from this type of testing, and if you are curious I very much encourage you to read the article “Stubs are not Mocks” written by Fowler on the topic. In any case, and as pointed out by Fowler, these type of testing is not exclusive of other, more traditional types of testing, and it should be understood as another tool in the testers toolkit



