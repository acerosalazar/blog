---
title: "Component Patterns"
date: 2018-07-29T18:20:45+02:00
description: "Based on Uncle Bob's Clean Architecture Book"
draft: true
---

I am working my way through the latest book of Uncle Bob: [Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure-ebook/dp/B075LRM681/ref=mt_kindle?_encoding=UTF8&me=&qid=1532881404). The book is full of useful applicable knowledge, and I highly recommend it to any software developer that has ever struggled to define how should his application be structured. 

One particular passage that I found interesting, and which took me a couple of reads to sink in, is the two chapters that talk about components.

A component is simply a group of classes that are released together.

Anyone tha has ever worked in a non trivial software development project would know splitting the application into several components is an old and tested strategy to maintain the architecture of the application/

What is architecutre you may I ask? I like the way uncle bob defines it: "architecture is a set of desitions that are made throught the project with the aim of preserving the productivity of the development team". Now productiviy here is a very overload concept but it includes the ease of development, ease of maintanability and ease of redability.

IN any case, the topic that I want to touch on today are the two sets of patterns that Robert C. Martin shares in his books.

The first three patterns aim to answer the question: what classes (replace by the unit of functionality of your programming language) should be included in a component? and the second question: what rules should be the relations between these components?

Right out of the gate, I encounter a shocker for me: "The components of your application are not defined by what the application does, but instead by how the application is developed.". This was a shock to me, because I always thought that the component structure of my applications was supposed to reflect part of the behavior of the app. Turns out that the components are just a map of developability and buildability of the application. That's it. 

Now back to the questions

### Component Composition

There are three patterns that dictacte what classes should be part of a components. These patterns are:

- Release Equivalence Principle (REP): 
Classes that are released together should be packed together
- Common Closure Principle
Classes that change together should be packed together
- Common Reuse Principle
Depend only on what you use

The first two principles, as Uncle Bob puts it. Are inclusive principles. Following them will lead to a monolith that contains all the classes of the application. This is fantastic in terms of developability – all the changes in the application are localized in a single component. However, this is not very good from the perspective of re-use. This is very good for example, when the project is just starting up, the code base is small and only small dev team is involved.

As the project matures, and more developers join the team two architectural goals start to gain momentum: extract reusable components, split the components to allow simpler development.

Now third principle is exclusive, meaning following this principle will lead to many small components with only one or two classes. The rational behind this principle is very similar to the Single Responsibility Principle (SRP) for classes. It tells you that if there are many components that depend on a single class of another component, then the component should be split in two: one component with the class, and another component with the other clases. Why? the reason is simple, you don't want depending components to have to integrate newer versions of the component when the class they are interested on does not change.

Obviously, as with many things in programming, this is a trade-off. On the one hand, having many small components is very good for stability, depending components only need to be recompiled if the classes the strictly depend upon chage. On the other hand, now developing the system has become a bit more complex because changes may span several components.


### Component Relations

Once you have figure out what classes go inside your components, the next step is to figure out what are the relations that should exist between these components. In this regard, Bob offers three advices:

- Acyclic Dependencies Principle:
Your dependency graph should be acyclic. Introducing cycles in your graph will be a maintenance nightmare, because every time you change your component, the depending component will change, and then you will have to change again. 

One way of breaking cycles is using somthing like the Dependency Inverison Principle but applied to components. First, you take the class that is creating the cycle, you define a protocl from it, and then you make the class in the other framework comform to it.

- Stable Dependencies Principle:
The aim of this principle is to help you ensure that volatile components stay volatile, while stable components remain stable. The definition used by uncle Bob for volatility is very clear: your component is maximally stable if many components depend on it, and it doesn't depend on anyone. Your component is maximally volatile if it depends on many components and no one depends on it. 

If you like graphs, this means that a sink node is maximally stable, whereas a fountain node is maximally volatile. 

The reasons behind this definition is very straight forward. Every incomming dependency is good reason **not** to change. Every outgoing dependency is a reason to change. In the first case, an incomming dependency is a reason not to change because changing may cause issues in the components that depend on your component. In the second case, an outgoing dependency is a reason to change because every time one of the underlying dependencies changes you may have to change as well.

The principle itself advises us to first identify which components are supposed to remain volatile (e.g the UI component) and which components are supposed to remain stable (e.g the core business logic). Once you have identified whic components are which, the next step is to make sure that their volatility or stability is not affected. 

How do you affect the volatility? easy make other component depend on in. Now is not so easy to change the compoenent –ie. you cannot just change that protocol, or remove that public API because client components may depend on it and the might break.

Same applies for stability. To break stability simply create an outgoing relation. Now everytime your dependency changes you may have to change your implementation as well.


These two concepts can be recasted as ease of change. A volatile component is a component that is changeable and should be easy to change. A stable component, in contrast, is not easy to change. Organizing your dendencies so that volatile components remain easy to change, and stable components remain stable is the aim of this principle.

- Abstract Dependencies Principle

The more stable the component the more abastract it should be. Why? because you want to allow certain flexibilty without break stability. This is just like the Open Closed Principle for classes. The open closed principle for classes, says that your class should be closed for modification but open for extensions –i.e your class API should be stable, but there should be a way to extend the API and tweak the behaviour. 

Maximally stable components are impractical. Requirements change, funtionalities need to be introduced. However, preserving some of that stability is very important to preserve the existing functionality and avoid regressions and new bugs. If the stable components are very abstract, all the sudden we can do both. We can keep our APIs stable, but we can also extend them when we need to.

What about the other case? The case of very abstract volatile classes? This is useless. Abstract classes add certain complexity with the design, and if the benefit is not clear, then the price is too high. Volatile components need to be easy to change. Making them abstract makes them harder to change. Volatile components should be concrete.

