---
title: "Performance Evaluation in Queues"
date: 2018-02-26T08:40:29+01:00
description: "Custom collections in Swift"
draft: true
---

### TL;DR

- Queues are a very useful *FIFO* data structure.
- They can be implemented in several ways –e.g using arrays and linked lists
- When creating custom collections in swift conforming to *Sequence* or *Collection* brings in a lot of useful features

### Introduction

Queues are a fundamental *FIFO* data structure. In their most basic implementation, a queue should support three operations:

```swift
protocol QueueType {
  associatedtype Element
  /// Number of elements in the queue
  var count: Int { get }
  /// Adds a new element to the back of the queue
  func enqueue(_ value: Element)
  /// Removes an element from the front of the queue
  func dequeue() -> Element?
}
```

Although the swift standard library does not include a *Queue* datatype, we can use some of the collections and protocols available in it to create our own implementation. In this post we will explore two possible implementations. The first implementation will be based on arrays, while the second one will use a completely custom *Linked List* implementation.

<!-- Is worth exploring the most naive implementation. An implementation based on a single array-->
Before diving into these implementations, is worth asking why can't we simply use the `append` and `remove(at:)` APIs offered by Arrays. After all, we could simply provide an implementation like the one shown below, and call it a day:

```swift
class Queue<T>: QueueType {
    private var array: [T] = []

    var count: Int  {
        return array.count
    }

    func enqueue(_ value: T) {
        array.append(value)
    }

    func dequeue() -> T? {
        guard count > 0 else { return nil }
        return array[0]
    }
}
```

> A proper implementation of this API should ensure constant space and time complexity.



- The time and space complexity of these operations should be O(1)
- There are several ways of implementing Queues
- In this post I will explore two of them. The first implementation is based on Arrays, while the second one uses a linked list data structure.

### Array Based Approach

- This implementation can only guarantee *amortized* O(1) complexity, which may be sufficient in most cases
- The implementation requires two arrays, a left array and a right array.
- Elements are added to the right array
- Elements will be removed from the left array
- Everytime the left array is empty we will make the left array equals to the reversed version of the right array. The right array will be emptied
- The reason this approach is amortized is because of the copy operation. This operation is NOT done upon every `dequeue`; instead is done every once in a while.


### Linked List Approach
- This implementation requires a bit more code, but does guarantee a O(1) time and space complexity
- More over, adding `Sequence` conformance is trivial
