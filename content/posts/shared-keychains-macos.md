---
title: "Shared Keychains in macOs"
description: "Using iCloud and ACLs for sharing secrets between macOS apps"
date: 2018-09-24T08:48:42+02:00
draft: true
---

<!--
* Sharing a keychain in iOS is fairly straight forward: 
	* Declare app-key-chain-groups
	 * Pass the key-chain group when storing your values
-->

There are two ways of sharing a keychain item between applications in macOS:

- Via shared keychains – which requires sandboxing and relies on the iCloud keychain
- Via ACLs

In this post I will present code for both options.

### Shared Keychains

* Declare app-key-chain-groups
* This means that the key will live inside of the iCloud keychain and will be synchronised across devices.
* Use the `kSecAttrSynchronizable as String: kCFBooleanTrue`, key pair as part of your read and write queries.
* Use the `kSecAttrAccessGroup as String: group as AnyObject`, key pair as part of your read and write queries.

```swift
extension KeychainError: Error {
    case fetchError
    case storeError
    case trustedAppError
}

func storeData(_ data: Data, 
              label: String,
              account: String,
              group: String) throws {
  let query: [String: AnyObject] = [
      kSecClass as String: kSecClassGenericPassword,
      kSecAttrLabel as String: label as AnyObject,
      kSecAttrAccount as String: account as AnyObject,
      kSecValueData as String: data as AnyObject,
      kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
      kSecAttrSynchronizable as String: kCFBooleanTrue,
      kSecAttrAccessGroup as String: group as AnyObject
   ]

   let resultCode = SecItemAdd(query as CFDictionary, nil)

   guard resultCode == noErr else {          
		throw KeychainError.storeError
    }
}

func fetchData(for account: String, group: String) throws -> Data? {
    let query: [String: AnyObject] = [
        kSecClass as String: kSecClassGenericPassword,
        kSecAttrAccount as String: account as AnyObject,
        kSecAttrSynchronizable as String: kCFBooleanTrue,
        kSecAttrAccessGroup as String: group as AnyObject
        kSecReturnData as String: kCFBooleanTrue,
        kSecMatchLimit as String: kSecMatchLimitOne
    ]

    var result: AnyObject?

    let resultCode = withUnsafeMutablePointer(to: &result) {
        SecItemCopyMatching(query as CFDictionary,
                            UnsafeMutablePointer($0))
    }
    guard resultCode == noErr else { 
        throw KeychainError.fetchError
    }

    return result as? Data
}

```

* [kSecAttrAccessGroup - Security | Apple Developer Documentation](https://developer.apple.com/documentation/security/ksecattraccessgroup) - Warning agains the usage in macOS
* [kSecAttrSynchronizable - Security | Apple Developer Documentation](https://developer.apple.com/documentation/security/ksecattrsynchronizable) Additional details about how to use it in macOS

### Access Control Lists

* Use the access control list API.

```swift
func storeData(_ data: Data, 
               account: String, 
               description: String, 
               paths: [String]) throws {
    var apps: []
    for path in paths {
        var app: SecTrustedApplication?
        let resultCode = path.withCString { 
            SecTrustedApplicationCreateFromPath($0, &app) 
        }
        guard result == noErr else { 
            throw KeychainError.trustedAppError
        }
        apps.append(app)
    }

    var access: SecAccess?
    SecAccessCreate(description as CFString, apps as CFArray, &access)

    let query: [String: AnyObject] = [
        kSecClass as String: kSecClassGenericPassword,
        kSecAttrAccount as String: account as AnyObject,
        kSecValueData as String: data as AnyObject,
        kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
        kSecAttrAccess as String: access as AnyObject
    ]
    let resultCode = SecItemAdd(query as CFDictionary, nil)
    guard resultCode == noErr else {          
        throw KeychainError.storeError
    }
}

func fetchData(for account: String) throws -> Data? {
    let queryLoad: [String: AnyObject] = [
        kSecClass as String: kSecClassGenericPassword,
        kSecAttrAccount as String: account as AnyObject,
        kSecReturnData as String: kCFBooleanTrue,
        kSecMatchLimit as String: kSecMatchLimitOne
    ]
    var result: AnyObject?
    let resultCode = withUnsafeMutablePointer(to: &result) {
        SecItemCopyMatching(queryLoad as CFDictionary, 
                            UnsafeMutablePointer($0))
    }
    guard resultCode == noErr else { 
        throw KeychainError.fetchError
    }
    
    return value
}
```

* [kSecAttrAccess - Security | Apple Developer Documentation](https://developer.apple.com/documentation/security/ksecattraccess)
* [Access Control Lists | Apple Developer Documentation](https://developer.apple.com/documentation/security/keychain_services/access_control_lists)
* [SecAccessCreate(_:_:_:) - Security | Apple Developer Documentation](https://developer.apple.com/documentation/security/1393522-secaccesscreate)
* [SecTrustedApplicationCreateFromPath(_:_:) - Security | Apple Developer Documentation](https://developer.apple.com/documentation/security/1400622-sectrustedapplicationcreatefromp)
* [1505.06836 Unauthorized Cross-App Resource Access on MAC OS X and iOS](https://arxiv.org/abs/1505.06836)
