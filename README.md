#### Contents

##### Checkout
```bash
git clone --recurse-submodules
```

##### `code`:
- Contains the source code referenced in the blog posts

##### `src`: 
- Contains the sources needed by Hugo for generating the site
- Is versioned in `github/acerosalazar/acerosalazar.github.src`

##### `src/public`: 
- Contains the generated files of Hugo
- Is a submodule of `src`
- Is versioned in `github/acerosalazar/acerosalazar.github.io`

#### `design`:
- Contains the graphic design of the blog

#### Workflow
- Add a new post running: `hugo new posts/[POST_NAME].md`
- Run `hugo server -D` for starting a local server with the site (including your drafts) 
- When ready to publish:
    - Mark the post final –i.e remove the `draft: true` line in the front-matter
    - From the root of the site run `hugo`
    - Commit changes to the `public` submodule to update the production site
    - Commit changes to the root repository
